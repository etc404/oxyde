import os

os.makedirs(os.path.expanduser("~/.config/oxyde/mod/"), exist_ok=True)
print(os.path.expanduser("~/.config/oxyde/mod/"), "created.")

if input("Link example program modules to directory? [y/n]\n") == "y":
    dirname = os.path.dirname(__file__)
    for directory in os.listdir(os.path.join(dirname, "example_modules")):
        if os.path.exists(os.path.join(os.path.expanduser("~/.config/oxyde/mod/"), directory)):
            os.remove(os.path.join(os.path.expanduser("~/.config/oxyde/mod/"), directory))
        os.symlink(os.path.join(dirname, "example_modules", directory), os.path.join(os.path.expanduser("~/.config/oxyde/mod/"), directory))
        print(os.path.join(dirname, "example_modules", directory), "linked.")
    print("Directories linked.")
else:
    print("Opted out.")
