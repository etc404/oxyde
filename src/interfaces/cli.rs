pub fn pss(message: &str, idx: &str, stack: &mut Vec<String>) -> Result<(), Box<dyn std::error::Error>> {
    println!("{}", message);
    stack.reverse();
    let idx = idx.replace(".", &format!("{}", stack.len() - 1)).parse::<usize>().unwrap();
    if stack.len() > idx {
        println!("{}", stack[idx]);
    } else {
        println!("WARNING: Attempted to print out of range.");
    }
    stack.reverse();
    Ok(())
}

pub fn psr(message: &str, range: &str, stack: &mut Vec<String>) -> Result<(), Box<dyn std::error::Error>> {
    println!("{}", message);
    stack.reverse();
    let bounds = range.to_owned();
    let bounds = bounds.replace(".", &format!("{}", stack.len() - 1));
    let bounds = bounds.split(",").collect::<Vec<&str>>();
    for stackitem in &stack[bounds[0].parse::<usize>().unwrap()..=bounds[1].parse::<usize>().unwrap()] {
        println!("{}", stackitem);
    }
    stack.reverse();
    Ok(())
}

pub fn inp(message: &str, stack: &mut Vec<String>) -> Result<(), Box<dyn std::error::Error>> {
    println!("{}", message);
    let mut input = String::new();
    std::io::stdin().read_line(&mut input)?;
    input = input.trim().parse().unwrap();
    stack.push(input);
    Ok(())
}