pub fn pss(message: &str, idx: &str, stack: &mut Vec<String>) -> Result<(), Box<dyn std::error::Error>> {
    stack.reverse();
    let idx = idx.replace(".", &format!("{}", stack.len() - 1)).parse::<usize>().unwrap();
    let mut child = std::process::Command::new("zenity")
        .args(vec!["--info", &format!("--text={}\n{}", message, stack[idx])])
        .spawn()
        .expect("Failed to display.");
    child.wait()?;
    stack.reverse();
    Ok(())
}

pub fn psr(message: &str, range: &str, stack: &mut Vec<String>) -> Result<(), Box<dyn std::error::Error>> {
    stack.reverse();
    let bounds = range.to_owned();
    let bounds = bounds.replace(".", &format!("{}", stack.len() - 1));
    let bounds = bounds.split(",").collect::<Vec<&str>>();
    let mut child = std::process::Command::new("zenity")
        .args(vec!["--info", &format!("--text={}\n{}", message, stack[bounds[0].parse::<usize>().unwrap()..=bounds[1].parse::<usize>().unwrap()].join("\n"))])
        .spawn()
        .expect("Failed to display.");
    child.wait()?;
    stack.reverse();
    Ok(())
}

pub fn inp(message: &str, stack: &mut Vec<String>) -> Result<(), Box<dyn std::error::Error>> {
    let output = std::process::Command::new("zenity")
        .args(vec!["--entry", &format!("--text={}", message)])
        .output()
        .expect("Failed to get user input.");
    stack.push(String::from_utf8_lossy(&output.stdout).trim().parse().unwrap());
    Ok(())
}