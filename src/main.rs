// Program modules provide tools and capabilities for the tool, such as a file search or program launcher.

// USAGE: oxyde <interface>
// Builtin Commands:
// h, help, -h, --help      Display this message
// l, list, -l, --list      Lists installed modules
// r, run, -r, --run [command]      Run command via argument

mod interfaces;

use std::fs;
use std::process::{Command, exit};
use regex::Regex;
use std::env;
use dirs::home_dir;

#[derive(Debug)]
enum StackOperation {
    POP,
    PUSH(String),
    SURFACE(usize) // descending id, eg: top of stack is 0
}

#[derive(Debug)]
enum Commands {
    Script(String),
    ReturnScript(String),
    Interface(String),
    Stack(StackOperation)
}

#[derive(Debug)]
struct CommandPattern {
    pattern: String,
    command: String
}

#[derive(Debug)]
struct CommandRoutine {
    name: String,
    calls: Vec<Commands>
}

#[derive(Debug)]
struct PModuleData {
    name: String,
    cpatterns: Vec<CommandPattern>,
    croutines: Vec<CommandRoutine>
}

#[derive(Debug)]
struct InputData {
    argument: String,
    routine: Vec<Commands>
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args: Vec<String> = env::args().collect();
    if args.len() > 1 {
        let arg = &args[1];
        if arg == "h" || arg == "help" || arg == "-h" || arg == "--help" {
            println!("USAGE: oxyde<interface>\nBuiltin Commands:\nh, help, -h, --help      Display this message\nl, list, -l, --list      Lists installed modules\nr, run, -r, --run [command]      Run command via argument");
        } else if arg == "l" || arg == "list" || arg == "-l" || arg == "--list" {
            println!("Program Modules:");
            for module in read_pmodules() {
                println!("{}", module.name);
                for pattern in module.cpatterns {
                    println!("├─ \"{}\" => {}", pattern.pattern, pattern.command);
                }
            }
        } else if arg == "r" || arg == "run" || arg == "-r" || arg == "--run" {
            if create_input_data(args[2..].join(" ")).routine.is_empty() {
                let mut tmpstack = vec![];
                interfaces::gui::psr("No matching routine found, dumping stack.", "0,.", &mut tmpstack)?;
            } else {
                execute_routine("gui", create_input_data(args[2..].join(" ")))?;
            }
        } else {
            if ["cli", "gui"].contains(&&**arg) {
                if args.len() > 2 {
                    if &args[2] == "r" || &args[2] == "run" || &args[2] == "-r" || &args[2] == "--run" {
                        if create_input_data(args[3..].join(" ")).routine.is_empty() {
                            let mut tmpstack = vec![];
                            interfaces::gui::psr("No matching routine found, dumping stack.", "0,.", &mut tmpstack)?;
                        } else {
                            execute_routine(arg, create_input_data(args[2..].join(" ")))?;
                        }
                    }
                } else {
                    let mut tmpstack: Vec<String> = vec![];
                    let arg = arg.as_str();
                    match arg {
                        "cli" => interfaces::cli::inp("Please Input Command:", &mut tmpstack),
                        "gui" => interfaces::gui::inp("Please Input Command:", &mut tmpstack),
                        _ => Ok(())
                    }?;
                    if create_input_data(tmpstack[0].clone()).routine.is_empty() {
                        interfaces::gui::psr("No matching routine found, dumping stack.", "0,.", &mut tmpstack)?;
                    } else {
                        execute_routine(arg, create_input_data(tmpstack[0].clone()))?;
                    }
                }
            } else {
                println!("Invalid arguments.");
                println!("USAGE: oxyde <interface>\nBuiltin Commands:\nh, help, -h, --help      Display this message\nl, list, -l, --list      Lists installed modules\nr, run, -r, --run [command]      Run command via argument");
            }
        }
    } else {
        let mut tmpstack: Vec<String> = vec![];
        interfaces::gui::inp("Please Input Command:", &mut tmpstack)?;
        if create_input_data(tmpstack[0].clone()).routine.is_empty() {
            interfaces::gui::psr("No matching routine found, dumping stack.", "0,.", &mut tmpstack)?;
        } else {
            execute_routine("gui", create_input_data(tmpstack[0].clone()))?;
        }
    }
    Ok(())
}

fn execute_routine(iface: &str, input: InputData) -> Result<(), Box<dyn std::error::Error>> {
    let mut stack = vec![input.argument];
    // Main Loop
    for call in input.routine {
        match call {
            Commands::Script(val) => {
                let val = val.replace('Δ', &stack[stack.len() - 1]);
                let mut child = Command::new("bash")
                    .args(vec!["-c", &val])
                    .spawn()
                    .expect("Failed to run command.");
                child.wait()?;
            }
            Commands::ReturnScript(val) => {
                let val = val.replace('Δ', &stack[stack.len() - 1]);
                let output = Command::new("bash")
                    .args(vec!["-c", &val])
                    .output()
                    .expect("Failed.");
                stack.push(String::from_utf8_lossy(&output.stdout).parse().unwrap());
            }
            Commands::Stack(val) => {
                // STACK OPERATIONS
                match val {
                    StackOperation::POP => {
                        if stack.is_empty() {
                            println!("WARNING: Attempted to pop empty stack.");
                        } else {
                            stack.pop();
                        }
                    }
                    StackOperation::PUSH(val) => {
                        stack.push(val);
                    }
                    StackOperation::SURFACE(idx) => {
                        if stack.len() > idx {
                            stack.push(stack[stack.len() - idx - 1].to_owned());
                            stack.remove(stack.len() - (idx + 2));
                        } else {
                            println!("WARNING: Stack surfacing out of range.")
                        }
                    }
                }
            }
            Commands::Interface(val) => {
                let segments = val.split("|").collect::<Vec<&str>>();
                match segments[0] { // Handle
                    "PSS" => {
                        match iface {
                            "cli" => interfaces::cli::pss(segments[1], segments[2], &mut stack)?,
                            "gui" => interfaces::gui::pss(segments[1], segments[2], &mut stack)?,
                            _ => {}
                        }
                    }
                    "PSR" => {
                        match iface {
                            "cli" => interfaces::cli::psr(segments[1], segments[2], &mut stack)?,
                            "gui" => interfaces::gui::psr(segments[1], segments[2], &mut stack)?,
                            _ => {}
                        }
                    }
                    "INP" => {
                        match iface {
                            "cli" => interfaces::cli::inp(segments[1], &mut stack)?,
                            "gui" => interfaces::gui::inp(segments[1], &mut stack)?,
                            _ => {}
                        }
                    }
                    _ => {}
                }
            }
        }
    }
    Ok(())
}

fn create_input_data(input: String) -> InputData {
    InputData {
        argument: argument_from_input(input.to_owned(), &match_to_routine(input.to_owned()).1),
        routine: routine_name_to_routine(match_to_routine(input.to_owned()).0)
    }
}

fn match_to_routine(input: String) -> (String, String) {
    let mut candidates = vec![];
    for module in read_pmodules() {
        for pattern in module.cpatterns {
            if Regex::new(&*format!("^{}$", pattern.pattern).replace("Δ", ".*")).unwrap().is_match(&input) {
                candidates.push((pattern.command, pattern.pattern));
            };
        };
    };
    if candidates.len() > 0 {
        candidates[0].to_owned()
    } else {
        (String::from("NULL"), String::from("NULL"))
    }
}

fn argument_from_input(input: String, pattern: &str) -> String {
    let mut newinput = input;
    for section in pattern.split('Δ') {
        newinput = newinput.replace(section, "");
    }
    newinput
}

fn routine_name_to_routine(name: String) -> Vec<Commands> {
    for module in read_pmodules() {
        for routine in module.croutines {
            if routine.name == name {
                return routine.calls;
            };
        };
    };
    vec![]
}

fn read_pmodules() -> Vec<PModuleData> {
    let mut pmodlist = vec![];
    let mut dir = home_dir().unwrap();
    dir.push(".config");
    dir.push("oxyde");
    dir.push("mod");
    // Read Program Modules (peek highest variable with Δ, script commands denoted with / (or $ to add output to stack), interface commands denoted with &, and stack operations denoted with ^)
    for directory in fs::read_dir(dir).unwrap() {
        let file = fs::read_to_string(directory.as_ref().unwrap().path()).unwrap();
        // Collect Module Lines
        let mut patterns = vec![];
        let mut routines = vec![];
        for line in file.split("\n") {
            if line.len() > 0 {
                if line.chars().next().unwrap() == '@' {
                    patterns.push(cpattern_from_string(line));
                } else {
                    routines.push(croutine_from_string(line));
                }
            }
        }
        pmodlist.push(PModuleData {
            name: format!("{:?}", directory.as_ref().unwrap().file_name())[1..directory.unwrap().file_name().len() - 3].parse().unwrap(),
            cpatterns: patterns,
            croutines: routines
        });
    }
    pmodlist
}

fn croutine_from_string(input: &str) -> CommandRoutine {
    let segments = input.split(':').collect::<Vec<&str>>();
    let mut calls = vec![];
    for call in &segments[1..] {
        if call.chars().next().unwrap() == '/' {
            calls.push(Commands::Script(call[1..].parse().unwrap()))
        } else if call.chars().next().unwrap() == '$' {
            calls.push(Commands::ReturnScript(call[1..].parse().unwrap()))
        } else if call.chars().next().unwrap() == '&' {
            calls.push(Commands::Interface(call[1..].parse().unwrap()))
        } else if call.chars().next().unwrap() == '^' {
            calls.push(
                if &call[1..=3] == "POP" {
                    Commands::Stack(StackOperation::POP)
                } else if &call[1..=4] == "PUSH" {
                    Commands::Stack(StackOperation::PUSH(call[5..].parse().unwrap()))
                } else if &call[1..=7] == "SURFACE" {
                    Commands::Stack(StackOperation::SURFACE(call[8..].parse().unwrap()))
                } else {
                    panic!();
                }
            );
        } else {
            panic!();
        }
    }
    CommandRoutine {
        name: segments[0].parse().unwrap(),
        calls
    }
}

fn cpattern_from_string(input: &str) -> CommandPattern {
    if input.split('|').collect::<Vec<&str>>().len() == 2 {
        CommandPattern {
            pattern: input.split('|').collect::<Vec<&str>>()[1].parse().unwrap(),
            command: input.split('|').collect::<Vec<&str>>()[0][1..].parse().unwrap()
        }
    } else {
        println!("Invalid command pattern: {}. Please correct to continue. Example: @command|pattern", input);
        exit(1);
    }
}