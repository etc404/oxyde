
# oxyde

A simple framework written in [Rust](https://www.rust-lang.org/). Meant for personal use, but open for adaptation by anyone interested.
## Usage

There are a couple different means of using oxyde.

**Method 1: -h, --help, h, help**

`cargo run [-h, --help, h, help]`

Oxyde will print the help menu

**Method 2: -r, --run, r, run**

`cargo run [-r, --run, r, run] <command>`

Oxyde will run with the command provided, skipping prompting the user for the command.

**Method 3: no arguments**

`cargo run`

Oxyde will run with the GUI interface by default, and will prompt you graphically to enter a command.

**Method 4: just interface**

`cargo run [cli, gui]`

Oxyde will start in the specified interface mode, and prompt the user for command input.

**Method 4: interface with run**

`cargo run [cli, gui] [-r, --run, r, run] <command>`

Oxyde will start in the specified interface mode, run the supplied command, and skip prompting the user for input.
## Installation

Download this project, and cd into the directory. Caution: [zenity](https://wiki.gnome.org/Projects/Zenity) is required to use the baked-in GUI interface.

To setup relevant folder layout for program modules (~/.config/oxyde/mod/...), run [the setup script](src/setup.py). If you would like to also install the example modules supplied, enter "y" when prompted by the setup script.
## Documentation
This tool's modular system is built around the use of "program modules", composed of files that outline:
+ The "routine" of a command, broken down as follows:
    + command handle:commands:broken:by:colons
    + "Δ" symbol is replaced with the highest item on the "execution stack". When the command is started, this is whatever argument the user might have supplied the program.
    + commands can be one of the following:
        + "stack" command, denoted by ^ (arguments, if applicable, separated by |)
            + `^POP`: remove highest item from execution stack
            + `^PUSH|item`: add string to execution stack
            + `^SURFACE|index`: move item at stack index (with highest at 0) to top of stack
        + "interface" command, denoted by &
            + `&PSS|message|index`: display message, and show element of stack at index (with highest at 0)
            + `&PSR|message|range`: display message, and show series of elements of stack in range (with highest at 0). range formatted as `start,end(inclusive)` and "." functioning as "end of stack". For instance, `&PSR|some message|0,.` would display the message "some message", as well as the entirety of the stack.
            + `&INP|message`: display message, and take text input from user. Input is added to the top of the executions stack.
        + "script" command, denoted by /
            + Simply runs the supplied command (for instance, `/thunar Δ`). Uses bash, so ~ substitution and the sort works for files.
        + "returnscript" command, denoted by $
            + Same as script command, but output from program is added to stack.
    + EX: `view:/thunar Δ`

+ The "patterns" of a command, which is how the framework matches what the user types to a module command. Written as `@\[command handle\]|pattern`, with the command handle being the short identifier defined in the relevant routine line. **Multiple patterns can exist for a single routine.** "Δ" symbol is the argument, and allows users to supply commands with data inline.
    + EX: `@view|view Δ`, `@view|please view Δ`

Example program module can be found [here](src/example_modules/os.prg).
## Feedback

For questions or feedback, please contact me at etc.anashi@pm.me
